import React from 'react'

const Tick = (props) =>  {
    return (
        <div className={'tick ' + (props.active ? "active " : null)+(props.done ? "done" : null)}></div>
    )
}

export default Tick;