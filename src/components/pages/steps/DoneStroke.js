import React from 'react'

const DoneStroke = () => { 
    return(
        <svg version="1.1" id="line_2" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px" width="50px" height="50px" className="done-line">
            <path className="path-vert" fill="#e4bf00" strokeWidth="0.5" stroke="#e4bf00" d="M30 0 v600 400"/>
        </svg>
    );
}

export default DoneStroke;