import React, {Component} from 'react'
import Tick from './Tick'
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import '../../../css/datepicker.css'
import $ from 'jquery'
import { DateRangePicker, SingleDatePicker, DayPickerRangeController } from 'react-dates';
class Step extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: "",
            subStepCounter: 0,
            isActive: null,
            changedProp: null,
            subSteps: this.props.value.subSteps
        }

        this.nextStep = this.nextStep.bind(this);
        this.handleButtonClick = this.handleButtonClick.bind(this);
        this.openCalendar = this.openCalendar.bind(this);
    }

    componentDidMount() {
            this.props.value.status === "active" 
            ? this.setState({isActive: true}) 
            : this.setState({isActive: false})
    }
    componentDidUpdate(prevProps){
        if(prevProps.changedProp !== this.props.changedProp){
            this.setState({          
                changedProp: this.props.changedProp
            });
        }
    }
    nextStep(i) {
        let temp = this.state.subSteps.slice();
        temp[i].selected = false;
        temp[i].done = true;
        temp[i+1].active = true;
        this.setState({subSteps: temp});
    }
    openCalendar() {
        $("input[name='startDate']").focus();
        console.log($("input[name='endDate']"));
        /*$("input[name='endDate']").bind('input', function(){
            console.log('INSIDEEEEEEEEEEEEEEEEEEE');
            let temp = this.state.subSteps.slice();
            temp[0].selected = false;
            temp[0].done = true;
            this.setState({subSteps: temp});
        })*/
        this.nextStep(0);

    }
    makeOffer() {
        //test data
        var propertyDataAccept = {
              pathname: "/accept/"+0,
              property: {
                id: 285729857,
                owner: {
                  name: 'Name Surname',
                  owned: 14,
                  sold: 6,
                  bought: 2
                },
                price: 4000000,
                deposit: 150000,
                diligence: 12,
                closing: 8,
                conditions: true,
                address: 'This is the address',
                financing: 'All cash',
                attorney: 'Name Surname'
              },
              state: {
                  from: this.props.location,
                  counterForm: false
              }

        }
        console.log(this.props.history);
        this.props.history.push(
            propertyDataAccept
        )
    }
    handleButtonClick(i) {
        let temp = this.state.subSteps.slice();
        temp[i].selected = !temp[i].selected;
        this.setState({subSteps: temp});
        switch(i) {
            case 0:
                this.setState({pickDate: true});
                this.openCalendar();
                break;
            case 1: 
                this.makeOffer();
                break;
        }
    }
    render() {
        var self = this;
        var state = this.state;
        const subSteps = this.props.value.subSteps.map(function(prop, i){
            return(
                <li key={i}>
                    <Tick
                        active={prop.active}
                        display={prop.display}
                        done={prop.done}
                    />
                    {
                        state.isActive 
                        ? <button 
                                disabled={!prop.active} 
                                className={
                                    (prop.display ? undefined : 'hide') + ' ' 
                                    + (prop.selected ? 'selected' : undefined)
                                } 
                                onClick={() => self.handleButtonClick(i)}
                                >{prop.full}
                            </button> 
                        : <button 
                                disabled={!prop.active} 
                                className={prop.selected ? 'selected' : undefined}>
                                {prop.short}
                        </button>
                    }    
                </li>
            )
        })
        return(
            <div className={"process-step " + this.props.value.status} onClick={this.props.onClick}>
                <div className="process-title">
                    <span className="step-number">{this.props.value.stepNumber}</span>
                    {this.props.value.stepTitle}
                </div>
                <div className="process-content">
                    <ul>
                        {subSteps}
                        {this.state.pickDate 
                            ? <DateRangePicker
                                startDateId="startDate"
                                endDateId="endDate"
                                startDate={this.state.startDate}
                                endDate={this.state.endDate}
                                orientation='vertical'
                                onDatesChange={({ startDate, endDate }) => { this.setState({ startDate, endDate })}}
                                focusedInput={this.state.focusedInput}
                                onFocusChange={(focusedInput) => { this.setState({ focusedInput })}}
                            />
                            : undefined
                        }
                    </ul>
                </div>
            </div>
        )
    }
}

export default Step;
