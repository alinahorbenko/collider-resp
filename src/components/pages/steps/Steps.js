import React, {Component} from 'react' 
import {Link} from 'react-router-dom'
import Step from './Step'

class Untitled extends Component {

    constructor(props) {
        super();
        this.state = {
            currentStep: 0,
            totalSteps: 4,
            steps: undefined
        }
        this.getData = this.getData.bind(this);
        this.moveToCard = this.moveToCard.bind(this);
        this.haveAttorney = this.haveAttorney.bind(this);
    }


    moveToCard() {
        this.props.history.push({
            pathname: "/steps/card",
            state: {
              from: this.props.location
            }
        })
    }
    haveAttorney() {
      this.props.history.push({
        pathname: "/steps/have-attorney",
        state: {
          from: this.props.location
        }
      })
    }
    getData = () => {
        this.setState({
            steps: [
              {
                stepNumber: 1,
                stepTitle: 'Tour & Offer',
                subSteps: [
                  {
                    full: 'Schedule tour',
                    short: 'Schedule tour',
                    active: true,
                    selected: false,
                    done: false
                  },
                  {
                    full: 'Make offer',
                    short: 'Make offer',
                    active: false,
                    selected: false,
                    done: false
                  },
                  {
                    full: 'Counter offer received',
                    short: 'COR',
                    active: false,
                    selected: false,
                    done: false
                  },
                  {
                    full: 'Offer accepted',
                    short: 'OA',
                    active: false,
                    selected: false,
                    done: false
                  }
                ],
                status: 'active'
                
              },
              {
                stepNumber: 2,
                stepTitle: '3rd party providers for buyers',
                subSteps: [
                  {
                    full: 'Broker Collider',
                    short: 'Broker Collider',
                    active: false,
                    done: false
                  },
                  {
                    full: 'Choose Lender',
                    short: 'Choose lender',
                    active: false,
                    done: false
                  },
                  {
                    full: 'Order Title Search',
                    short: 'OTS',
                    active: false,
                    done: false
                  },
                  {
                    full: 'Choose Insurance',
                    short: 'CI',
                    active: false,
                    done: false
                  }
                ],
                status: 'disabled'
              },
              {
                stepNumber: 3,
                stepTitle: 'Contract',
                subSteps: [
                  {
                    full: 'Contract Issued',
                    short: 'CI',
                    active: false,
                    done: false
                  },
                  {
                    full: 'Contract Comments',
                    short: 'CC',
                    active: false,
                    done: false
                  },
                  {
                    full: 'Contract Ready',
                    short: 'CR',
                    active: false,
                    done: false
                  },
                  {
                    full: 'Contract Signed',
                    short: 'CI',
                    active: false,
                    done: false
                  }
                ],
                status: 'disabled'
              },
              {
                stepNumber: 4,
                stepTitle: 'Closing',
                subSteps: [
                  {
                    full: 'Broker Collider',
                    short: 'Broker Collider',
                    active: false,
                    done: false
                  },
                  {
                    full: 'Choose lender',
                    short: 'Choose lender',
                    active: false,
                    done: false
                  },
                  {
                    full: 'Order title search',
                    short: 'OTS',
                    active: false,
                    done: false
                  },
                  {
                    full: 'Choose insurance',
                    short: 'CI',
                    active: false,
                    done: false
                  }
                ],
                status: 'disabled'
              }
            ]
          });
  }

    componentDidMount() {
        this.getData();
  }
    
    goToNextStep = () => {
        this.setState({currentStep: this.state.currentStep + 1});
    }

    render() {
        if(this.state.steps === undefined) {
            return null;
        }
        else {   
            return(
                <section style={{backgroundColor: "black"}} className="section purchase-process" data-anchor="purchase" id="purchase">
                {/*<Link to={this.props.location.state.from} className="back"><i className="fa fa-angle-left"></i>Back</Link>*/}
                    <div className="container center center-xy">
                        <div className="process-wrapper">
                        <Step value={this.state.steps[0]}
                              history={this.props.history} />
                        <Step value={this.state.steps[1]} />
                        <Step value={this.state.steps[2]} />
                        <Step value={this.state.steps[3]} />
                        </div>
                    </div>
                </section>
            )
        }
    }
}

export default Untitled;