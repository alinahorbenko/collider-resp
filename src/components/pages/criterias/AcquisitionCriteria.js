import React, {Component} from 'react'

class AcquisitionCriteria extends Component {

    constructor(props) {
        super(props);
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.goBack = this.goBack.bind(this);
        this.state = {
            neighbourhoodOptions: [
                "Westchester Square",
                "Lenox Hill",
                "Upper East Side",
                "Fordham",
                "Pelham Parkway"
            ],
            boroughOptions: [
                "Bronx",
                "Manhattan",
                "Brooklyn",
                "Queens",
                "Staten Island"
            ],
            propertyTypeOptions: [
                "Residential",
                "Community facilities",
                "Commercial",
                "Manufacturing",
                "With attic allowance",
                "Land"
            ]
        }
    }
    handleChange(e) {
        const target = e.target;
        this.setState({
          [target.name]: target.value
        });
      }
    
      handleSubmit(e) {
        e.preventDefault();
        console.log(this.state);
        if(parseInt(this.state.max) < parseInt(this.state.min)) {
            this.setState({showError: true});
            return
        }
        this.props.history.push({
            pathname: "/login",   
            state: {
                type: "buyer", 
                result: 42,
                from: this.props.location
            }
        });
    }

    handleKeyPress(e) {
        const pattern = /[0-9\ ]/;
        let inputChar = String.fromCharCode(e.charCode);
        if (!pattern.test(inputChar)) {
            e.preventDefault();
        }
    }
    goBack(e) {
        e.preventDefault();
        this.props.history.push('/');
    }
    render() {
        const borough = this.state.boroughOptions.map(function(prop, i) {
            return (
                <option key={i}>
                    {prop}
                </option>
            );
        }, this);
        const propertyTypes = this.state.propertyTypeOptions.map(function(prop, i){
            return(
                <option key={i}>
                    {prop}
                </option>
            );
        }, this)
        return(
            <section className="section acquisition-criteria" data-anchor="acquisition-criteria" id="acquisition-criteria">
                <div className="container center center-xy">
                <a href="" className="back" onClick={this.goBack}><i className="fa fa-angle-left"></i>Back</a>
                    <h1 className="title center-self">Tell us more about your acquisition criteria</h1>
                    <div className="form-container">
                        <form id="acquisition" onSubmit={this.handleSubmit} autoComplete="off">
                            <label htmlFor="borough">Borough</label>
                            <select type="text" name="borough" id="borough" onChange={this.handleChange} defaultValue="" required>
                                <option disabled></option>
                                {borough}
                            </select>
                            <label htmlFor="neighbourhood">Neighbourhood</label>
                            <select type="text" name="neighbourhood" id="neighbourhood" onChange={this.handleChange} defaultValue="" required>
                                <option disabled></option>
                                <option>{this.state.neighbourhoodOptions[this.state.boroughOptions.indexOf(this.state.borough)]}</option>
                            </select>
                            <label htmlFor="prop-type">Property type</label>
                            <select type="text" name="property" id="prop-type" onChange={this.handleChange} defaultValue="" required>
                                <option disabled></option>
                                {propertyTypes}
                            </select>
                            <label htmlFor="cap-rate">Minimal cap rate</label>
                            <input 
                                type="text" 
                                name="rate" 
                                id="cap-rate" 
                                onChange={this.handleChange}    
                                onKeyPress={this.handleKeyPress} 
                                onPaste={this.handleKeyPress}  
                                defaultValue="" 
                                required/>
                            <label htmlFor="min">Deal volume</label>
                            <div className="form-line" id="deal-volume">
                                <input 
                                    type="text" 
                                    name="min" 
                                    placeholder="Min" 
                                    id="min" 
                                    onChange={this.handleChange} 
                                    onKeyPress={this.handleKeyPress} 
                                    onPaste={this.handleKeyPress} 
                                    defaultValue="" 
                                    required/>
                                <input 
                                    type="text" 
                                    name="max" 
                                    placeholder="Max" 
                                    id="max" 
                                    onChange={this.handleChange} 
                                    onKeyPress={this.handleKeyPress} 
                                    onPaste={this.handleKeyPress} 
                                    defaultValue="" 
                                    required/>
                            </div>
                            <button className="bg-yellow center-self" type="submit">Send</button>
                            <br/>
                            {this.state.showError ? <span className="error center-self">Please, check deal volume values</span> : undefined }
                        </form>
                    </div>
                </div>
            </section>
        )
    }
}

export default AcquisitionCriteria;