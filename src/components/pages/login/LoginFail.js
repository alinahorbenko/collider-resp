import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import $ from 'jquery'
class LoginFail extends Component {

    constructor(props){
        super();
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {}
    }

    handleChange(e) {
        const target = e.target;
        /*if([target.name] == "files[]") {
                var fileList = $("input[type='file'")[0].files;
                if (fileList.length > 2) {
                    alert("You can select only 2 images");
                    target.value = [];
                    return false;
                } 
            $('.upload-btn').addClass('upload-success');
        }  */

        //when one button - replace target name with 'files[]'
        if([target.name].toString().indexOf("file") !== -1) {
            if($('input[name="'+target.name.toString()+'"]')[0].files.length > 0) {
                var fileInput = $('input[name="' + target.name.toString()  + '"]');
                fileInput.parent().find('.upload-btn')
                .addClass('upload-success');
                fileInput.parent().find('input.success').prop('checked', true);
            }
            else {
                var fileInput = $('input[name="' + target.name.toString()  + '"]');
                fileInput.parent().find('.upload-btn')
                .removeClass('upload-success');
                fileInput.parent().find('input.success').prop('checked', false);
            }
        } 
        this.setState({
          [target.name]: target.value
        });
    }
    handleSubmit(e) {
        e.preventDefault();
        console.log('submitted');
        this.props.history.push({
            pathname: "/bill-check",
            state: {
                file: "",
                from: this.props.location
            }
        });
    }

    render() {
        return(
            <section className="section login-fail" data-anchor="login-fail" id="login-fail">
            <div className="container center center-xy">
            <Link to={this.props.location.state.from} className="back"><i className="fa fa-angle-left"></i>Back</Link>
                <h1 className="title center-self bold uppercase">Thank you!</h1>
                <h1 className="center-self">Provided e-mail do not match our records.<br/>
                    Please submit property water bill that confirms your identity</h1><br/><br/><br/>
                <div className="form-container">
                    <form id="login-fail" onSubmit={this.handleSubmit}>
                        <div className="btn-container center-self">
                            <button className="upload-btn water-bill half">
                                <i className="fa fa-paperclip"></i>Water bill
                            </button>
                            <input className="half" type="file" name="file" required onChange={this.handleChange}/>
                            <input id="check" type="checkbox" className="success" defaultChecked={false} disabled/>
                        </div><br/><br/>
                        <button className="bg-yellow center-self half" type="submit">Submit</button>
                    </form> 
                </div>
                </div>
        </section>
        ); 
    }
}

export default LoginFail;