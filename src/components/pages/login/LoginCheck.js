import React, {Component} from 'react'
import {Link} from 'react-router-dom'
class LoginCheck extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        console.log(this.props);
        return(
            <section className="section login-check" data-anchor="login-fail" id="login-fail">
            <div className="container center center-xy">
            <Link to={'/'} className="back"><i className="fa fa-angle-left"></i>Back</Link>
                <h1 className="title center-self bold uppercase">Thank you!</h1>
                <h1 className="center-self">We are checking your water bill.<br/>
                    You will get an email notification once we are done.</h1><br/>
                </div>
        </section>
        ); 
    }
}

export default LoginCheck;