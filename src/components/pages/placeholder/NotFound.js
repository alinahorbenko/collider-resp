import React, {Component} from 'react'

class NotFound extends Component {
    render() {
        return(
            <section class="section not-found" data-anchor="analyze" id="analyze">
                <div class="container center center-xy center-self">
                            <h2><span>404</span></h2>
                            <p>Page not found</p>
                </div>
            </section>
        )
    }
}

export default NotFound;