import React, {Component} from 'react'
import {Link} from 'react-router-dom'
class ProvidedInfo extends Component {
    constructor(props) {
        super();
        this.state = {
            min: 2.4,
            max: 5.2,
            buyers: 32
        }
        this.goNext = this.goNext.bind(this)
    }

    goNext(e) {
        console.log(this.props.location)
        e.preventDefault();
        this.props.history.push({
            pathname: '/asking-price',
            state: {
                from: this.props.location
            }
            
        })
    }
    render() {
        console.log(this.props.location.state);
        return(
            <section className="section property-worth" data-anchor="property-worth" id="property-worth">
                <div className="split-container">
                <Link to={this.props.location.state.from} className="back"><i className="fa fa-angle-left"></i>Back</Link>
                    <div className="split left">
                        <div className="container full-width">
                            <div className="main-titles-container">
                                <div className="main-titles">
                                    <div className="yellow-square"></div>
                                    <h1 className="bold">Based on<br/> provided info</h1><br/><br/>
                                    <p className="half">
                                        your property is worth in the <span className="bold">${this.state.min} - {this.state.max}M range</span><br/>
                                        Collider has identified {this.state.buyers} most likely buyers
                                    </p>
                                    <div className="button-container">
                                <a href="" className="button yellow">Comparables</a>
                                <Link to='' onClick={this.goNext} className="button yellow">Sell with us?</Link>
                            </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div className="split right">
                        <div className="button-container bottom-center">
                            <a href="" className="button yellow">Comparables</a>
                            <Link to='' onClick={this.goNext} className="button yellow">Sell with us?</Link>
                        </div>   
                    </div>   
                </div>
            </section>
        )
    }
}

export default ProvidedInfo;