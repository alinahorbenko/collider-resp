import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import PercentageChart from './PercentageChart'
import $ from 'jquery'

class BuyerOffers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            properties: ""
        }

        this.getData = this.getData.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.moveToSteps = this.moveToSteps.bind(this);
    }

    getData() {
       /* $.ajax({
            url: "../json-test-data/buyerOffers.json",
            type: 'GET',
            dataType: 'json',
            success: function(parsed_json){
                this.setState({properties: parsed_json});
            }.bind(this)
        })*/
        this.setState({properties :[
            {
            
                percentage: 97,
                neighbourhood: 'Upper East Side',
                sf: 2500,
                type: 'multifamily',
                cap: 4.5
            },
            {
                percentage: 94,
                neighbourhood: 'Upper West Side',
                sf: 1200,
                type: 'studio',
                cap: 6
            },
            {
                percentage: 86,
                neighbourhood: 'Upper East Side',
                sf: 2500,
                type: 'multifamily',
                cap: 4.5
            },
            {
                percentage: 72,
                neighbourhood: 'Upper West Side',
                sf: 1200,
                type: 'studio',
                cap: 6
            }
          ]})
    }
    handleClick() {
        this.props.history.push({
            pathname: '/docusign'
        })
    }

    moveToSteps() {
            this.props.history.push({
                pathname: "/steps",
                state: {
                    from: this.props.location
                }
            });
        }
    
    componentDidMount() {
        this.getData();     
    }

    render() {
        if (!this.state.properties) {
            return null;
        }
        else {
            const properties = this.state.properties.map(function(prop, i) {

                return (
                    <div key={i} class="info-box">
                        <PercentageChart percentage={prop.percentage} />
                        <div class="info center-self">
                            {prop.neighbourhood}, +-{prop.sf}SF, {prop.type}, {prop.cap}% going in cap.
                        </div>
                        <button class="bg-yellow accept" onClick={this.moveToSteps}>Agreement</button>
                    </div>
                );
            }, this);

            return(
                <section className="buyer-offers">
                    <div className="container center center-xy">
                    <Link to={this.props.location.state.from} className="back"><i className="fa fa-angle-left"></i>Back</Link>
                            <h1 className="title center-self">We have identified 25 properties</h1>
                            <p className="center-self">that match your acquisition criteria</p>
                            <div className="properties center-self">
                                {properties}
                        </div>
                    </div>
                </section>
            )
        }
    }
    
}

export default BuyerOffers;