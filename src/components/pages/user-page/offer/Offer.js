import React, { Component } from 'react';
import {BrowserRouter as Router, Link} from 'react-router-dom'

function SubmitBtn(props) {
    return(
        <button onClick={props.onClick} className="submit-btn bg-yellow center-self">Submit offer</button>
    )
}

function AcceptCounterBtn(props) {
    return(
        <div>
        <button style={{width: 328 +'px'}} onClick={props.onClick} className="submit-btn bg-yellow center-self">Accept offer</button><br/><br/>
        </div>
    )
}

function SubmitNewCounter(props) {
    return(
        <div>
        <button onClick={props.onClick} className="submit-btn bg-yellow center-self">Submit new counter offer</button><br/><br/>
        <h6 className="center">Accepting this Offer, your Attorney will be <br/>notified to prepare a contract</h6>
        </div>
    )
}

function BackBtn() {
    return(
        <Link to='/user_123' className="back" >
            <i className="fa fa-chevron-left"></i>Back to deal process
        </Link>
    )
}

class Offer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: "",
            isCounterSelected: false
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.selectCounter = this.selectCounter.bind(this);
    }

    handleSubmit() {
        this.props.history.push({
            pathname: "/user_123"
        })
    }

    selectCounter(e) {
        e.preventDefault();
        this.setState({isCounterSelected: true})
    }
    render() {
        console.log(this.props.location.counterForm);
        return (
            <section className="section offer" id="offer">
            <BackBtn />
                <div className={"form-container half center-xy offer " + ( this.state.isCounterSelected ? "counter-offer" : undefined)}>
                    <form className="center-xy">
                        <table className="offer-data  center">
                        <tbody>
                            <tr>
                                <td>Property address:</td>
                                <td><input type="text" defaultValue={this.props.location.property.address} readOnly required/></td>
                            </tr>
                            <tr>
                                <td>Price:</td>
                                <td><input type="text" className={this.props.location.counterForm ? "changed" : undefined} defaultValue={("$" + this.props.location.property.price)} required/></td>
                            </tr>
                            <tr>
                                <td>Deposit:</td>
                                <td>
                                    <input type="text" defaultValue={("$" +  this.props.location.property.deposit)} required/>
                                </td>
                            </tr>
                            <tr>
                                <td>Closing:</td>
                                <td><input type="text" defaultValue={ this.props.location.property.closing} required/></td>
                            </tr>
                            <tr>
                                <td>Financing:</td>
                                <td><input type="text" defaultValue={ this.props.location.property.financing} required/></td>
                            </tr>
                            <tr>
                                <td>Special Conditions:</td>
                                <td><input type="text" defaultValue={ this.props.location.property.conditions ? "yes" : "no"} required/></td>
                            </tr>
                            <tr>
                                <td>Attorney info:</td>
                                <td><input type="text" defaultValue={ this.props.location.property.attorney} readOnly required/></td>
                            </tr>
                            <tr>
                                <td colSpan="2" className="center-self">
                                        <React.Fragment>
                                            <AcceptCounterBtn onClick={this.handleSubmit.bind(this)}/>
                                            <SubmitNewCounter onClick={this.selectCounter.bind(this)}/>  
                                        </React.Fragment>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </section>
        );
    }
}

export default Offer;