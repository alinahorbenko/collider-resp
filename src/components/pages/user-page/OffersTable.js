import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import {Link} from 'react-router-dom'
import Offer from './offer/Offer'

class OffersTable extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const propertyItems = this.props.properties.map(function(prop, i) {

            const propertyDataAccept = {
                counterForm: false,
                pathname: "/view-offer/"+i,
                property: {
                    owner: prop.owner,
                    address: prop.address,
                    price: prop.price,
                    deposit: prop.deposit,
                    closing: prop.closing,
                    financing: prop.financing,
                    consitions: prop.conditions,
                    attorney: prop.attorney
                }
            }
            const propertyDataCounter = {
                counterForm: true,
                pathname: "/counter/"+i,
                property: {
                    owner: prop.owner,
                    address: prop.address,
                    price: prop.price,
                    deposit: prop.deposit,
                    closing: prop.closing,
                    financing: prop.financing,
                    consitions: prop.conditions,
                    attorney: prop.attorney
                }
            }
            return (
            <tr key={i}>
                <td>
                    <div className="buyer-info">
                        <h3 className="buyer-name">{prop.owner.name}</h3>
                        <ul>
                            <li>{prop.owner.owned} properties owned</li>
                            <li>{prop.owner.sold} property sold in last 12 months</li>
                            <li>{prop.owner.bought} properties bought in last 12 months</li>
                        </ul>
                    </div>
                </td>
                <td>${prop.price}</td>
                <td>${prop.deposit}</td>
                <td>{prop.diligence} days</td>
                <td>{prop.closing} days</td>
                <td>{prop.conditions ? "yes" : "no"}</td>
                <td>
                    <div className="accept-counter">
                        <Link className="accept" to={propertyDataAccept}>View</Link>
                    </div>
                </td>
            </tr>
            );
        }, this);

        return(
            <table className="offers-list">
                <thead>
                    <tr>
                        <td>Property</td>
                        <td>Price</td>
                        <td>Deposit</td>
                        <td>Diligence</td>
                        <td>Closing</td>
                        <td>Conditions</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    {propertyItems}  
                </tbody>
            </table>
        );
    }
}

export default OffersTable;